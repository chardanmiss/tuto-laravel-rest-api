<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'ddn' => '1969-04-02',
            'web_site_url' => "http://jessemichaels.bigcartel.com/",
            'facebook_url' => "https://www.facebook.com/jesse.michaels.142",
            'linkedin_url' => "www.linkedin/jesse-michaels",
            'user_id' => 2,
        ]);

        DB::table('profiles')->insert([
            'ddn' => '1952-08-21',
            'web_site_url' => "http://www.theclash.com/",
            'facebook_url' => "https://www.facebook.com/JoeStrummerOfficial/",
            'linkedin_url' => "www.linkedin/joe-strummer",
            'user_id' => 3,
        ]);

        DB::table('profiles')->insert([
            'ddn' => '1966-11-25',
            'web_site_url' => "http://timtimebomb.com/",
            'facebook_url' => "https://www.facebook.com/TimTimebombMusic/",
            'linkedin_url' => "www.linkedin/tim-timebomb",
            'user_id' => 4,
        ]);
    }
}
