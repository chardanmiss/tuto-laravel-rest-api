<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetProfileFromUserWithAnAlreadyExistingProfile()
    {
        // Arrange
        // Act
        $response = $this->get('/api/users/2/profile');
        // Assert
        $response->assertJsonFragment(['ddn' => '1969-04-02',
                                        'web_site_url' => "http://jessemichaels.bigcartel.com/",
                                        'facebook_url' => "https://www.facebook.com/jesse.michaels.142",
                                        'linkedin_url' => "www.linkedin/jesse-michaels",
                                        'user_id' => 2]);
        $response->assertStatus(200);
    }

    public function testGetProfileInexistingUser()
    {
        $response = $this->get('/api/users/-1/profile');
        $response->assertStatus(404);
    }

    public function testGetInexistingProfileFromExistingUserShouldCreateNewProfile()
    {
        //User 4 does not have a profile
        $response = $this->get('/api/users/5/profile');
        $response->assertJsonFragment(['user_id' => 5]);
        $response->assertStatus(201);
    }

    public function testPutOnAnInexistingProfileShouldCreateNewProfileWithSpecifiedValues()
    {
        //User 5 does not have a profile
        $response = $this->put('/api/users/5/profile',
                                ['ddn' => '1962-02-06',
                                'web_site_url' => "http://gunsnroses.com/",
                                'facebook_url' => "https://www.facebook.com/gunsnroses/",
                                'linkedin_url' => "www.linkedin/axl-rose",]);

        $response->assertJsonFragment(['ddn' => '1962-02-06',
                                        'web_site_url' => "http://gunsnroses.com/",
                                        'facebook_url' => "https://www.facebook.com/gunsnroses/",
                                        'linkedin_url' => "www.linkedin/axl-rose",
                                        'user_id' => 5]);
        $response->assertStatus(200);
    }

    public function testPutOnAnExistingProfileShouldUpdateProfileWithSpecifiedValues()
    {
        //User 4 does not have a profile
        $response = $this->put('/api/users/1/profile',
                                    ['ddn' => '1912-02-06',
                                    'web_site_url' => "http://puttest.com/",
                                    'facebook_url' => "https://www.facebook.com/puttest/",
                                    'linkedin_url' => "www.linkedin/puttest",]);

        $response->assertJsonFragment(['ddn' => '1912-02-06',
                                        'web_site_url' => "http://puttest.com/",
                                        'facebook_url' => "https://www.facebook.com/puttest/",
                                        'linkedin_url' => "www.linkedin/puttest",
                                        'user_id' => 1]);
        $response->assertStatus(200);
    }

    public function testUpdatingProfileWithAnInvalidDateOfBirth()
    {
        $response = $this->put('/api/users/1/profile',
                                ['ddn' => 'not_an_actual_date'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["ddn" => ["The ddn is not a valid date."]]);
        $response->assertStatus(422);
    }

    public function testUpdatingProfileWithAnInvalidWebsiteUrl()
    {
        $response = $this->put('/api/users/1/profile',
                                ['web_site_url' => 3],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["web_site_url" => ["The web site url must be a string."]]);
        $response->assertStatus(422);
    }

    public function testUpdatingProfileWithAnInvalidFacebookUrl()
    {
        $response = $this->put('/api/users/1/profile',
                                    ['facebook_url' => 3],
                                    ['Accept' => 'application/json']);
        $response->assertJsonFragment(["facebook_url" => ["The facebook url must be a string."]]);
        $response->assertStatus(422);
    }

    public function testUpdatingProfileWithAnInvalidLinkedinUrl()
    {
        $response = $this->put('/api/users/1/profile',
                                ['linkedin_url' => 3],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["linkedin_url" => ["The linkedin url must be a string."]]);
        $response->assertStatus(422);
    }
}
